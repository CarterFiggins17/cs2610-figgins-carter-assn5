from django.urls import path
from . import views

urlpatterns = [
    path('', views.unitconv, name='unitconv'),
	path('init', views.init, name='init')
]