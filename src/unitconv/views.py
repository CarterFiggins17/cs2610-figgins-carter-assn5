from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.http import JsonResponse
from unitconv.models import Unit
from django.urls import reverse


# Create your views here.

def unitconv(request):
	#prints out Get request
	for key in request.GET:
		print(f"{key} => {request.GET[key]}")
	#Finds out what was in the URL given
	if 'value' in request.GET: 
		if 'to' in request.GET:
			if 'from' in request.GET:
				#gets info from url
				weight = request.GET['value']
				unit = request.GET['to']
				From = request.GET['from']
				factors = Unit.objects.all
				try:
					#converts units given
					value = convert(weight,factors)
					response = {
						'weight' : weight,
						'value' : value,
						'unit' : unit,
						'from' : From,
					}
				except: 
						response = {'error' : 'Value is not a number'}
			else: 
				response ={'error' : "Unit that you are changing does not work"}
		else: 
			response ={'error' : "Unit does not work"}

	else:
		response = {
			'error': 'You must supply a parameter "weight" ',
			}
			
	return JsonResponse(response)
	

	
def convert(weight, unit):
	#For now only t_oz can add more
	t_oz = Unit.objects.get(name = "t_oz")
	return float(weight) * t_oz.unit
	

	
	
	#Run to get database the units
def init(request):
	t_oz = 14.5833
	#makes sure you dont run two inits :)
	if(Unit.objects.filter(name='t_oz').exists()):
		print("Cant make two init!!!")
		return HttpResponseRedirect(reverse('gold'))
	
	unit = Unit(unit = t_oz, name = "t_oz")
	unit.save()
	return HttpResponseRedirect(reverse('gold'))

